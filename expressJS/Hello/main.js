var express = require('express');
var views = require('./views');
var app = express();

app.get('/', function(req, res){
    res.send('Hello, World!');
});

app.get('/:name', views.sayHello);

app.listen(8000, function(){
    console.log('Server listening at port 8000');
    console.log('To stop press CTRL+C');
});
