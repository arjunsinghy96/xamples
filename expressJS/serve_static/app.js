var express = require('express');
var path = require('path');
var app = express();

app.use(express.static('public'));

app.get('/', function(req, res){
    res.sendFile(path.join(__dirname, 'public/html/index.html'));
});

app.listen(4000, function(){
    console.log('Listening to 4000');
    console.log('CTRL+C to stop');
});
