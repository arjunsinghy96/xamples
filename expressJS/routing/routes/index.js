var express = require('express')
var router = express.Router();

router.use(function(req, res, next){
    console.log(req.method + ' ' + req.path + ' ' + Date.now());
    next();
});

router.get('/', function(req, res){
    res.send('Home page');
});

router.get('/:user', function(req, res){
    res.send('Dashboard of ' + req.params.user);
});

router.get('/:user/friends', function(req, res){
    res.send('Friends of ' + req.params.user);
});

router.get('/:user/posts', function(req, res){
    res.send('Posts of ' + req.params.user);
});

module.exports = router;
