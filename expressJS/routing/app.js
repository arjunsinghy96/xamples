var express = require('express');
var routes = require('./routes/index');
var app = express();

app.use(routes);

app.listen(3000, function(){
    console.log('Server listening at port 3000');
    console.log('To stop the server user CTRL+C');
});
