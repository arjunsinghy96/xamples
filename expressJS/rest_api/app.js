var express = require('express');
var apiv1 = require('./api/message');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({"extended": false}));

app.get('/', function(req, res){
    res.json({"error": false,
        "messageApi": "localhost:3000/api/v1/message",
        "message": "Hello World"});
});

app.use('/api/v1/', apiv1);

app.use(function(err, req, res){
    console.log(err);
});

app.listen(3000, function(){
    console.log("Listening to port 3000")
});
