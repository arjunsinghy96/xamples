var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/apiDB');

var Schema = mongoose.Schema

var message_schema = new Schema(require('./message_schema.json'));
var user_schema = new Schema(require('./user_schema.json'));

exports.User = mongoose.model('User', user_schema);
exports.Message = mongoose.model('Message', message_schema);
