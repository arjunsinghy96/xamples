var express = require('express');
var Message = require('../models/db').Message;
var router = express.Router();

router.use(function(req, res, next){
    console.log(req.method, req.path);
    next();
});

router.get('/message/', function(req, res){
    var response = {};
    Message.find({}).select("-_id -__v").exec(function(err, data){
        if(err){
            response = {"error": true, "message": "Error fetching Data"};
        }
        else{
            response = {"error": false, "data": data};
        }
        res.json(response);
    });
});

router.post('/message/', function(req, res){
    var msg = new Message();
    var response = {};
    msg.sent_at = Date.now();
    msg.recipient = req.body.recipient;
    msg.sender = req.body.sender;
    msg.contents = req.body.contents;
    msg.save(function(err){
        if(err){
            response = {"error":true, "message": "message cannot be saved"};
        }
        else{
            response = {"error": false, "message": "Message successfully saved"};
        }
        res.json(response);
    });
});

module.exports = router;
